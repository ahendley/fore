﻿# FO Rendering Engine

For any application that needs to provide reports to its users, the big question for any developer is what reporting method do I use? You could generate Excel files, or HTML documents, Crystal Reports, flat text files or some other method.  What about to PDF? The foRendererEngine is designed specifically to render XML data 
using XSL stylesheets to produce PDF documents. Using PDF format provides a rich viewing document that will preserve your desired formatting, no matter who views it or which PDF viewing tool they may use.

This text will help explain how the foRendererEngine can be incorporated into your application and used for generating PDF based reports. The document will explain how to establish a new library for storing all the components for your reports and how your application can then interact with the rendering engine.

## Table of Contents

- [Overview](#overview)
- [Usage](#Usage)
    - [Creating a new reports library](#creating-a-new-reports-library)

## Overview
The way in which the rendering engine is used can be 
explained by this simple sequence diagram:

![Sequence diagram](images/sequence.png)

1.  Once the main application has been launched, it would initialise the available reports which are defined in a configuration file.
2.  Next is to ensure that the embedded resource files are extracted to disk for reference by the rendering engine.
3.  The rendering engine then needs some properties set so it knows what it is meant to do and where to find certain things.
4.  Then the application can retrieve the list of discovered reports for usage in its own interface.
5.  And also use the list of discovered reports for requesting reports to be generated.

## Usage

### Creating a new reports library
> [Provide a concise description of the context for this process or workflow, including any requirements or conditions that are relevant. Repeat this entire section for each major workflow or process.]

#### Step 1 - Setup of new library
> [Provide a concise description of the context for this sub-process or workflow, including any requirements or conditions that are relevant.]

##### Procedures for Step 1
1.  In Visual Studio, create a new Project making sure that the framework selected is set to version 4 only, not the client profile option. This will act as the container for all your report objects. 
2.  Next add a new reference to the library file, foRendererEngine.dll.
3.  If you want to include logging, include the log4net reference library as well as the foRendererEngine uses this for dumping information about what it is doing.

#### Step 2 – Your first report implementation
> [Provide a concise description of the context for this sub-process or workflow, including any requirements or conditions that are relevant.]

##### Procedures for Step 2
1. Now add a new class file and enter a name that will describe your new report (eg: SampleReport.cs).
2. After the class declaration, add an implementation of the National.Reporting.foRendererEngine.ReportBase class.
3. As the ReportBase class implements the National.Reporting.foRendererEngine.IReport interface, there are four methods that have been implemented in the base class all of which are overloads of the method GetXml.  The only override that you need to implement on your child class is MemoryStream GetXml().
> **Note:**
> The other three GetXml overloads accept data source connection settings which are stored in protected local variables of the ReportBase and then call the GetXml() method which you have just overridden with your own implementation.

4. So that the foRendererEngine can identify your new report class, an attribute ReportAttribute needs to be added with the three properties of ReportName, DataType and Stylesheet set.  For now enter a meaningful name for the report, set the data type to None and name the stylesheet reference to a similar name as the class file created (eg: SampleReport.xslt). This will be explained later.

    New report class code example:

```
	using National.Reporting.foRendererEngine;
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Text;
	using System.Xml;
		
	namespace National.Reporting.Reports
	{
		[Report(ReportName = "Sample Report"
			, DataType = "None"
			, Stylesheet = "SampleReport.xslt")]
		public class SampleReport : ReportBase
		{
			public override MemoryStream GetXml()
			{
			}
		}
	}
```
5. As this is just a sample report, we shall just create an XmlWriter and create some sample XML output. For a proper business implementation that would normally access some sort of database, the connection settings information passed via the other overload GetXml methods, we can use here as well. More on that later.

    An example of the code using the XmlWriter inside your override method GetXml() can be found in the appendices.	

    That is pretty much all that is needed for the class implementation of your new report. 

#### Step 3 – Adding report parameters
If your report requires data input whether it be from a user or application coded, you can add a properties to your new report class and use this set information in your XML output.

##### Procedures for Step 3
1. In the new report class, add a new public string property called SubTitle.
2. So the application that is using the report knows if the report has report parameters, we need to add an attribute to the new property element of the class. Above the new property add the attribute ReportParameter which has the properties of Name, Description and DefaultValue.

    Code for new report parameter:

```
	[ReportParameter(Name="Sub Title"
	    , Description="Enter the report sub title"
	    , DefaultValue="[sub title goes here]")]
	public string SubTitle { get; set; }
``` 
#### Step 4 – Presenting the report XML data for users
Next we need to create the stylesheet that will control the presentation of the XML data that will be returned from our new report class created above.

##### Procedures for Step 4
1. Now add a new XSLT file to your project and enter the stylesheet reference name that was used when defining the class attributes above (eg: SampleReport.xslt).
2. Once the file has been created, change the Build Action property of the file to Embedded Resource.
3. Next, replace the content of this new stylesheet file with the code example found in the appendices.
4. If you look at the first few lines down from the start of the stylesheet code, there are three references to additional stylesheets in a sub folder called Components called:
	- Components/Formatting.xslt
	- Components/ReportHeader.xslt
	- Components/ReportFooter.xslt
	- From the appendices, create these additional files.
5. Also for these files, change the Build Action property of the files to Embedded Resource.
6. Looking further into the ReportHeader.xslt stylesheet, there is at least one more external reference to an image file located in a sub folder called Images:
    - Images/generic-logo.png

    This image file can be any file you like or save the one located in the appendices.
    
7. Also for this file, change the Build Action property of the files to Embedded Resource.

#### Step 5 – Image handling
One handy feature of the Renderer Engine is that if your reports assembly contains an implementation of the National.Reporting.foRendererEngine.IImageHandler interface, the engine will search for this when the reports are loaded.  If your report library does not contain this, then it has its own implementation.

The interface itself is fairly simple that contains two properties, one for pointing the image handler to the root folder of where to find the images and the other to indicate whether to cache the image in the running applications memory space for the life time of the running application. The only method is to return the bytes of the found image.  

The interface model is as follows:

```
	namespace National.Reporting.foRendererEngine
	{
		public interface IImageHandler
		{
			string RootFolder { get; set; }
			bool CacheImages { get; set; }
	
			byte[] GetImageData(string value);
		}
	}
```
##### Procedures for Step 5
1. Create a new public class in your reports library called ImageHandler.cs.
2. Implement the interface National.Reporting.foRendererEngine.IImageHandler.
3. Copy the code example from the appendices to implement your own image handler class.

Looking at the code, you will notice that within the byte[] GetImageData(string) method that it checks for the presence of the value passed for a PieChart or a LineChart. To facilitate the generation these charts you can add a reference to a charting library and from the data in the value password, generate these on the fly. What this does is provide a de-coupled solution of having the charting functionality embedded within the renderer engine.

This class can be in any form you like, but the example here is a complete working approach.

### Final library structure
In the end, you should have a project structure that should now include the following:

**Reporting classes**
* SampleReport.cs
* ImageHandler.cs
  
**Reporting stylesheets** (embedded resources)

* SampleReport.xslt
* Components\Formatting.xslt
* Components\ReportFooter.xslt
* Components\ReportHeader.xslt
  
**Reporting images** (embedded resources)

* Images\generic-logo.png
* Charting classes
* Charting\LineChart.cs
* Charting\PieChart.cs

**Additional references**

* foRendererEngine
* log4net
* netchartdir (optional charting library)