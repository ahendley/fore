﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Application.Reporting.FoRE;
using System.Reflection;
using System.IO;
using log4net;
using System.Diagnostics;

namespace RendererWrapper
{
	class Program
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(Program));

		static void Main(string[] args)
		{
			try
			{
				Log.Info("Initialising available reports;");
				AvailableReports.Initialise();

				Log.Info("Known available reports;");
				foreach (ReportItem _reportItem in AvailableReports.Reports)
				{
					Console.WriteLine("Report: {0}", _reportItem.Name);
					Console.WriteLine("  Key: {0}", _reportItem.Key);
					Console.WriteLine("  Type: {0}", _reportItem.Type.FullName);
					Console.WriteLine("  Data Type: {0}", _reportItem.DataType);
					Console.WriteLine("  Stylesheet: {0}", _reportItem.Stylesheet);
					Console.WriteLine("  Parameters:");
					foreach (string _key in _reportItem.Parameters.Keys)
						Console.WriteLine("    {0} (Name:{1}; Description:{2}; DefaultValue:{3})", _key, _reportItem.Parameters[_key].Name, _reportItem.Parameters[_key].Descrption, _reportItem.Parameters[_key].Value);

					IReport _report = (IReport)Activator.CreateInstance(_reportItem.Type);
					Console.WriteLine("  IReport instance: {0}", (_report != null ? "Yes" : "No"));

					Console.WriteLine("  XML:");
					Console.WriteLine(_report.GetXml());
				}

				foreach (Assembly _assembly in AvailableReports.Assemblies)
				{
					Console.WriteLine("{0}", _assembly.FullName);

					string _commonPath = AvailableReports.GetCommonPath(_assembly);
					Console.WriteLine("  Common path: {0}", _commonPath);
				}

				Console.WriteLine();
				Console.ReadKey(true);

				string _workingFolder = Path.Combine(System.Environment.CurrentDirectory, "Reports");
				string _outputFolder = Path.Combine(System.Environment.CurrentDirectory, "Output");

				AvailableReports.ExtractFiles(_workingFolder);

				if (!Directory.Exists(_outputFolder))
					Directory.CreateDirectory(_outputFolder);

				Console.WriteLine();
				Console.WriteLine("Start sample report.");
				Console.ReadKey(true);

				RendererEngine.WorkingFolder = _workingFolder;
				RendererEngine.CacheImagesInMemory = false;
				RendererEngine.SaveXMLOutput = true;
				RendererEngine.SaveXSLFOOutput = true;

				foreach (ReportItem _reportItem in AvailableReports.Reports)
				{
					if (_reportItem.Key.Equals("sample"))
					{
						string _filename = string.Format("{0}.pdf", System.Guid.NewGuid().ToString());
						string _outputFilename = Path.Combine(_outputFolder, _filename);

						using (FileStream _output = new FileStream(_outputFilename, FileMode.Create, FileAccess.Write, FileShare.Write))
						{
							RendererEngine.ImagesFolder = Path.GetDirectoryName(_reportItem.Stylesheet);
							//RendererEngine.Process(_xmlStream, _reportItem.Stylesheet, _output);
							RendererEngine.Process(_reportItem, _output);
						}

						ProcessStartInfo _startInfo = new ProcessStartInfo(_outputFilename);
						_startInfo.Verb = "open";
						Process.Start(_startInfo);

						break;
					}
				}
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Log.Error(ex);

				Console.ReadKey(true);
			}
		}

	}
}
