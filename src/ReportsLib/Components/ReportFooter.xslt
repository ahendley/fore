﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

	<xsl:template name="ReportFooter">
		<fo:table width="100%" border-collapse="separate" table-layout="fixed">
			<fo:table-column column-width="proportional-column-width(35)"/>
			<fo:table-column column-width="proportional-column-width(30)"/>
			<fo:table-column column-width="proportional-column-width(35)"/>
			<fo:table-body>
				<fo:table-row padding-top="2mm">
					<fo:table-cell text-align="left">
						<xsl:call-template name="Footer">
							<xsl:with-param name="value" select="Footer/Left"/>
						</xsl:call-template>
					</fo:table-cell>
					<fo:table-cell text-align="center">
						<xsl:if test="Footer/Middle != &quot;&quot; ">
							<xsl:call-template name="Footer">
								<xsl:with-param name="value" select="Footer/Middle"/>
							</xsl:call-template>
						</xsl:if>
						<xsl:if test="Footer/Middle = &quot;&quot; ">
							<fo:block font-family="Arial" font-size="8pt">
								Page <fo:page-number/>
							</fo:block>
						</xsl:if>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<xsl:call-template name="Footer">
							<xsl:with-param name="value" select="Footer/Right"/>
						</xsl:call-template>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
</xsl:stylesheet>
