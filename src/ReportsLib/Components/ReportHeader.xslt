﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

	<xsl:template name="ReportHeader">
		<fo:table width="100%" border-collapse="separate" table-layout="fixed">
			<fo:table-column column-width="proportional-column-width(35)"/>
			<fo:table-column column-width="proportional-column-width(30)"/>
			<fo:table-column column-width="proportional-column-width(35)"/>
			<fo:table-body>
				<fo:table-row padding-top="2mm">
					<fo:table-cell text-align="left">
						<xsl:call-template name="Header">
							<xsl:with-param name="value" select="./Header/Left"/>
						</xsl:call-template> 
					</fo:table-cell>
					<fo:table-cell text-align="center">
						<xsl:call-template name="Header">
							<xsl:with-param name="value" select="./Header/Middle"/>
						</xsl:call-template>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:external-graphic height="20mm" space-before.optimum="0pt" space-after.optimum="0pt">
							<xsl:attribute name="src">Images/generic-logo.png</xsl:attribute>
						</fo:external-graphic>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
</xsl:stylesheet>
