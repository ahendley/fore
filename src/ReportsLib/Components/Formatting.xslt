﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">


	<xsl:template name="Footer">
		<xsl:param name="value"/>

		<fo:block font-family="Arial" font-size="8pt">
			<xsl:value-of select="$value"/>
		</fo:block>
	</xsl:template>

	<xsl:template name="H1">
		<xsl:param name="value"/>

		<fo:block font-family="Arial" font-size="20pt" text-align="left" padding-before="6pt" padding-after="6pt">
			<xsl:value-of select="$value"/>
		</fo:block>
	</xsl:template>

	<xsl:template name="H2">
		<xsl:param name="value"/>
		
		<fo:block font-family="Arial" font-size="18pt" text-align="left" padding-before="6pt" padding-after="6pt">
			<xsl:value-of select="$value"/>
		</fo:block>
	</xsl:template>

	<xsl:template name="Header">
		<xsl:param name="value"/>

		<fo:block font-family="Arial" font-size="8pt">
			<xsl:value-of select="$value"/>
		</fo:block>
	</xsl:template>

	<xsl:template name="P">
		<xsl:param name="value"/>

		<fo:block font-family="Arial" font-size="10pt" text-align="left" padding-before="0pt" padding-after="6pt">
			<xsl:value-of select="$value"/>
		</fo:block>
	</xsl:template>

	<xsl:template name="Title">
		<xsl:param name="value"/>

		<fo:block font-family="Arial" font-size="24pt" text-align="center" padding-before="12pt" padding-after="24pt">
			<xsl:value-of select="$value"/>
		</fo:block>
	</xsl:template>


</xsl:stylesheet>

