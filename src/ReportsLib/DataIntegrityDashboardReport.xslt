﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:output method="xml" version="1.0" omit-xml-declaration="no" indent="yes"/>

	<xsl:include href="Components/ReportFooter.xslt"/>

	<xsl:template match="DataIntegrityDashboardReport">
		<fo:root>
			<!-- Prepare to call page margin config component -->
			<fo:layout-master-set>

				<fo:simple-page-master master-name="A4-portait" page-height="297mm" page-width="210mm" margin-top="10mm" margin-bottom="10mm" margin-left="22mm" margin-right="15mm">
					<fo:region-body margin-top="0mm" margin-bottom="0mm"/>
					<fo:region-before extent="0mm"/>
					<fo:region-after extent="10mm"/>
				</fo:simple-page-master>
				<fo:page-sequence-master master-name="sequence-A4-portrait">
					<fo:repeatable-page-master-reference master-reference="A4-portait"/>
				</fo:page-sequence-master>
			</fo:layout-master-set>

			<!-- Layout margins  -->
			<fo:page-sequence master-reference="sequence-A4-portrait">
				>
				<!--<fo:page-sequence master-reference="sequence-A4-portrait"  >>-->
				<fo:static-content flow-name="xsl-region-before">
				</fo:static-content>
				<fo:static-content flow-name="xsl-region-after">
					<xsl:call-template name="ReportFooter"/>
				</fo:static-content>

				<!-- Main section of the report 					-->
				<fo:flow flow-name="xsl-region-body" font-family="Arial" font-style="normal" font-size="10pt" text-align="left" wrap-option="wrap">
					<xsl:call-template name="ReportBody"/>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>

	<xsl:template name="ReportBody">
		<fo:block font-weight="bold" font-size="18pt" text-align="center" padding-after="12pt">
			<xsl:value-of select="Report/Body/Title" />
		</fo:block>

	</xsl:template>
	
</xsl:stylesheet>
