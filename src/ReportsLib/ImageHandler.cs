﻿using log4net;
using Application.Reporting.FoRE;
using Application.Reporting.Reports.Charting;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System;

namespace Application.Reporting.Reports
{
	public class ImageHandler: IImageHandler
	{

		#region Fields

		private static readonly ILog Log = LogManager.GetLogger(typeof(ImageHandler));
		private static Dictionary<string, object> ImagesCache = new Dictionary<string, object>();

		#endregion

		#region Constructor

		public ImageHandler()
		{ }

		#endregion

		#region Properties

		public string RootFolder { get; set; }
		public bool CacheImages { get; set; }

		#endregion

		#region Get Image Data

		public byte[] GetImageData(string value)
		{
			Log.DebugFormat("Getting image with value ({0})", value);

			MemoryStream _memoryStream = new MemoryStream();
			string _localFilename = Path.Combine(this.RootFolder, value);
			string _cacheKey = _localFilename.ToLower();

			if (value.StartsWith("PieChart") || value.StartsWith("LineChart"))
			{
				string[] _parts = value.Split(new char[] { '?' }, StringSplitOptions.RemoveEmptyEntries);
				string _chartType = _parts[0];
				string _dataString = _parts[1];

				if (_parts.Length > 2)
				{
					for (int _index = 2; _index < _parts.Length; _index++)
						_dataString += string.Format("?{0}", _parts[_index]);
				}

				if (_dataString.Equals("XVal=&YVal="))
					throw new Exception(String.Format("There is no data to generate a chart: ({0})", value));

				if (_chartType.StartsWith("LineChart"))
					_memoryStream = LineChart.GenerateStream(_dataString);
				else if (_chartType.StartsWith("PieChart"))
					_memoryStream = PieChart.GenerateStream(_dataString);
			}
			else if (this.CacheImages)
			{
				if (ImagesCache.ContainsKey(_cacheKey))
					_memoryStream = (MemoryStream)ImagesCache[_cacheKey];
				else
				{
					_memoryStream = GetLocalImageFile(_localFilename);
					ImagesCache.Add(_cacheKey, _memoryStream);
				}
			}
			else
				_memoryStream = GetLocalImageFile(_localFilename);

			return _memoryStream.ToArray();

		}

		private static MemoryStream GetLocalImageFile(string filename)
		{
			MemoryStream _stream = new MemoryStream();

			using (Image _image = Image.FromFile(filename))
			{
				_image.Save(_stream, ImageFormat.Png);
			}

			return _stream;
		}

		#endregion

	}
}
