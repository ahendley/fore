﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:output method="xml" version="1.0" omit-xml-declaration="no" indent="yes"/>

	<!-- Include component files here -->
	<xsl:include href="./Components/Formatting.xslt"/>
	<xsl:include href="./Components/ReportHeader.xslt"/>
	<xsl:include href="./Components/ReportFooter.xslt"/>

	<!-- Begin main report logic -->
	<xsl:template match="SampleReport">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
			<!-- Prepare to call page margin config component -->
			<fo:layout-master-set>
				<fo:simple-page-master master-name="A4-portait" page-height="297mm" page-width="210mm" margin-top="10mm" margin-bottom="10mm" margin-left="20mm" margin-right="20mm">
					<fo:region-body margin-top="22mm" margin-bottom="12mm"/>
					<fo:region-before extent="20mm"/>
					<fo:region-after extent="10mm"/>
				</fo:simple-page-master>
				<fo:page-sequence-master master-name="sequence-A4-portrait">
					<fo:repeatable-page-master-reference master-reference="A4-portait"/>
				</fo:page-sequence-master>
			</fo:layout-master-set>

			<!-- Layout margins  -->
			<fo:page-sequence master-reference="sequence-A4-portrait">
				<fo:static-content flow-name="xsl-region-before">
					<xsl:call-template name="ReportHeader"/>
				</fo:static-content>
				<fo:static-content flow-name="xsl-region-after">
					<xsl:call-template name="ReportFooter"/>
				</fo:static-content>

				<!-- Main section of the report -->
				<fo:flow flow-name="xsl-region-body" font-family="Arial" font-style="normal" font-size="10pt" text-align="left" wrap-option="wrap">
					<xsl:call-template name="ReportBody"/>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>

	<xsl:template name="ReportBody">
		<xsl:call-template name="Title">
			<xsl:with-param name="value" select="Body/Heading1"/>
		</xsl:call-template>

		<xsl:for-each select="Body/Paragraphs/Paragraph">
			<xsl:call-template name="H1">
				<xsl:with-param name="value">Paragraph <xsl:value-of select="position()"/></xsl:with-param>
			</xsl:call-template>
			<xsl:call-template name="P">
				<xsl:with-param name="value" select="."/>
			</xsl:call-template>
		</xsl:for-each>
		
	</xsl:template>

</xsl:stylesheet>