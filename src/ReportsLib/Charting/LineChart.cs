using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Application.Reporting.Reports.Charting
{
    public class LineChart
    {

        #region Constants

        private const int NumberOfPlasteringRetries = 100;
        private static readonly System.Drawing.Font LineChartLabelFont = new System.Drawing.Font("Arial Bold", 24);
        private static readonly System.Drawing.SolidBrush FirstLinePlasteringBrush = new System.Drawing.SolidBrush(System.Drawing.Color.White);
        private static readonly System.Drawing.SolidBrush SecondLinePlasteringBrush = new System.Drawing.SolidBrush(System.Drawing.Color.White);

        private const int ChartWidth = 1600;
        private const int ChartHeight = 800;
        private const int RightCornerWidth = 250;
        private const int RightCornerHeight = 40;
        private const int BottomRibbonHeight = 19;

        private const double PaddingPercentageFactor = 50.0;

        #endregion

        #region Generate

        public static MemoryStream GenerateStream(string values)
        {
            System.Drawing.Bitmap _image = LineChart.GenerateBitmap(values);
            MemoryStream _memoryStream = new MemoryStream();

            _image.Save(_memoryStream, System.Drawing.Imaging.ImageFormat.Png);

            return _memoryStream;
        }

        /// <summary>
        /// Uses the dotNETCharting .NET component.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static System.Drawing.Bitmap GenerateBitmap(string values)
        {
			//if (values.StartsWith("XVal="))
			//{
			//    System.Drawing.Bitmap _chartImage = null;
			//    string _trimmedValues = values.Remove(0, 5);
			//    int _posYVal = _trimmedValues.IndexOf("&YVal=");

			//    if (_posYVal > 0)
			//    {
			//        string[] _yValues = _trimmedValues.Substring(_posYVal + 6).Split(new char[1] { '|' });

			//        if (_yValues.Length <= 0)
			//        {
			//            throw new Exceptions.ReportServerException(String.Format("A line chart request string has an error. Provided Y-values '{0}' do not contain any data"
			//                , _trimmedValues.Substring(_posYVal + 6)));
			//        }

			//        double[] _yValueDoubles = new double[_yValues.Length];
			//        double _result;
			//        int _yValueIndex = 0;

			//        foreach (string _yValue in _yValues)
			//        {
			//            if (double.TryParse(System.Web.HttpUtility.UrlDecode(_yValue), System.Globalization.NumberStyles.Float, null, out _result))
			//                _yValueDoubles[_yValueIndex] = _result;
			//            else
			//            {
			//                throw new Exceptions.ReportServerException(String.Format("A line chart request string has an error. Provided Y-values '{0}' can not be converted to double type. The erroneous value is '{1}'"
			//                    , _trimmedValues.Substring(_posYVal + 6), _yValue));
			//            }

			//            _yValueIndex++;
			//        }

			//        string[] _xValues = _trimmedValues.Substring(0, _posYVal).Split(new char[1] { '|' });

			//        if (_xValues.Length != _yValues.Length)
			//            throw new Exceptions.ReportServerException(String.Format("The number of X-values ({0}) is not equal to the number of Y-values ({1})", _xValues.Length, _yValues.Length));

			//        dotnetCHARTING.Chart _mainChart = null;
			//        dotnetCHARTING.SeriesCollection _seriesCollection = null;
			//        dotnetCHARTING.Series _series = null;
			//        dotnetCHARTING.Series _fakeSeries = null;

			//        try
			//        {
			//            _mainChart = new dotnetCHARTING.Chart();
			//            _mainChart.ImageFormat = dotnetCHARTING.ImageFormat.Png;
			//            _mainChart.DefaultSeries.Type = dotnetCHARTING.SeriesType.Line;
			//            _mainChart.ChartArea.XAxis.Label.Text = String.Empty;
			//            _mainChart.ChartArea.XAxis.Line.Width = 4;
			//            _mainChart.ChartArea.XAxis.Line.Color = System.Drawing.Color.Black;
			//            _mainChart.ChartArea.XAxis.TickLabel.Font = LineChart.LineChartLabelFont;
			//            _mainChart.ChartArea.XAxis.TickLabelMode = dotnetCHARTING.TickLabelMode.Angled;
			//            _mainChart.ChartArea.XAxis.TickLabel.Text = "%Name";
			//            _mainChart.ChartArea.YAxis.Scale = dotnetCHARTING.Scale.Range;
			//            _mainChart.ChartArea.YAxis.Line.Width = 4;
			//            _mainChart.ChartArea.YAxis.Line.Color = System.Drawing.Color.Black;
			//            _mainChart.ChartArea.YAxis.TickLabel.Font = LineChart.LineChartLabelFont;
			//            _mainChart.Use3D = false;
			//            _mainChart.ShadingEffect = false;
			//            _mainChart.UseFile = false;
			//            _mainChart.DefaultSeries.DefaultElement.ShowValue = true;
			//            _mainChart.LegendBox.Position = dotnetCHARTING.LegendBoxPosition.None;
			//            _mainChart.ChartArea.ClearColors();
			//            _mainChart.ChartArea.YAxis.ShowGrid = true;
			//            _mainChart.ChartArea.YAxis.GridLine.Width = 2;
			//            _mainChart.ChartArea.YAxis.GridLine.Color = System.Drawing.Color.Black;
			//            _mainChart.Width = ChartWidth;
			//            _mainChart.Height = ChartHeight;
			//            _seriesCollection = new dotnetCHARTING.SeriesCollection();
			//            _series = new dotnetCHARTING.Series();
			//            _series.Name = string.Empty;
			//            _series.Line.Width = 9;
			//            _series.Line.Color = System.Drawing.Color.Black;

			//            int _valuesIndex = 0;
			//            double _minYValue = double.MaxValue;
			//            double _maxYValue = double.MinValue;

			//            foreach (string _xValue in _xValues)
			//            {
			//                dotnetCHARTING.Element _element = new dotnetCHARTING.Element();
			//                _element.Name = _xValue;
			//                _element.YValue = _yValueDoubles[_valuesIndex];
			//                _element.SmartLabel.Text = string.Empty;
                            
			//                _series.Elements.Add(_element);

			//                _minYValue = Math.Min(_minYValue, _element.YValue);
			//                _maxYValue = Math.Max(_maxYValue, _element.YValue);
                            
			//                _valuesIndex++;
			//            }

			//            // decorate main series and add it to series collection
			//            _series.DefaultElement.ForceMarker = false;

			//            for (int _index = 0; _index < _series.Elements.Count; _index++)
			//            {
			//                _series.Elements[_index].Color = System.Drawing.Color.White;
			//                _series.Elements[_index].Transparency = 0;
			//            }

			//            _seriesCollection.Add(_series);

			//            if (_yValueDoubles.Length > 1)
			//            {
			//                _fakeSeries = new dotnetCHARTING.Series();
			//                _fakeSeries.Name = String.Empty;
			//                _fakeSeries.Line.Width = 0;
			//                _fakeSeries.Line.Color = System.Drawing.Color.Empty;

			//                dotnetCHARTING.Element _firstFakeElement = new dotnetCHARTING.Element();
			//                _firstFakeElement.Name = _xValues[0];
			//                _firstFakeElement.YValue = _minYValue - (_maxYValue - _minYValue) * LineChart.PaddingPercentageFactor * 0.005;
			//                _firstFakeElement.SmartLabel.Text = string.Empty;
			//                _firstFakeElement.Color = System.Drawing.Color.Empty;

			//                _fakeSeries.Elements.Add(_firstFakeElement);
                            
			//                dotnetCHARTING.Element _secondFakeElement = new dotnetCHARTING.Element();
			//                _secondFakeElement.Name = _xValues[1];
			//                _secondFakeElement.YValue = _maxYValue + (_maxYValue - _minYValue) * LineChart.PaddingPercentageFactor * 0.005;
			//                _secondFakeElement.SmartLabel.Text = string.Empty;
			//                _secondFakeElement.Color = System.Drawing.Color.Empty;
                            
			//                _fakeSeries.Elements.Add(_secondFakeElement);
                            
			//                _fakeSeries.DefaultElement.ForceMarker = false;
                            
			//                _seriesCollection.Add(_fakeSeries);
			//            }

			//            _mainChart.SeriesCollection.Add(_seriesCollection);

			//            for (int _count = 0; _count < 10; _count++)
			//            {
			//                try
			//                {
			//                    _chartImage = _mainChart.GetChartBitmap();
			//                    break;
			//                }
			//                catch (System.NullReferenceException)
			//                {
			//                    System.Threading.Thread.Sleep(10);
			//                }
			//                catch
			//                {
			//                    break;
			//                }
			//            }
			//        }
			//        finally
			//        {
			//            _seriesCollection = null;
			//            _series = null;

			//            if (_mainChart != null)
			//            {
			//                _mainChart.Dispose();
			//                _mainChart = null;
			//            }
			//        }

			//        using (System.Drawing.Graphics _canvas = System.Drawing.Graphics.FromImage(_chartImage))
			//        {
			//            for (Int16 rr = 0; rr < LineChart.NumberOfPlasteringRetries; rr++)
			//            {
			//                try
			//                {
			//                    if (rr == (LineChart.NumberOfPlasteringRetries - 1))
			//                    {
			//                        _canvas.FillRectangle(new System.Drawing.SolidBrush(System.Drawing.Color.White), LineChart.ChartWidth - LineChart.RightCornerWidth, 0, LineChart.RightCornerWidth, LineChart.RightCornerHeight);
			//                        _canvas.FillRectangle(new System.Drawing.SolidBrush(System.Drawing.Color.White), 0, LineChart.ChartHeight - LineChart.BottomRibbonHeight, LineChart.ChartWidth, LineChart.BottomRibbonHeight);
                                    
			//                        break;
			//                    }

			//                    _canvas.FillRectangle(LineChart.FirstLinePlasteringBrush, LineChart.ChartWidth - LineChart.RightCornerWidth, 0, LineChart.RightCornerWidth, LineChart.RightCornerHeight);
			//                    _canvas.FillRectangle(LineChart.SecondLinePlasteringBrush, 0, LineChart.ChartHeight - LineChart.BottomRibbonHeight, LineChart.ChartWidth, LineChart.BottomRibbonHeight);
                                
			//                    break;
			//                }
			//                catch (System.InvalidOperationException ex)
			//                {
			//                    if (ex.Message.IndexOf("The object is currently in use elsewhere") > 0)
			//                        System.Threading.Thread.Sleep(10);
			//                    else
			//                        break;
			//                }
			//                catch
			//                {
			//                    break;
			//                }
			//            }
			//        }

			//        return _chartImage;
			//    }
			//    else if (_posYVal == 0)
			//        throw new Exceptions.ReportServerException("There are no data to generate a chart");
			//    else
			//        throw new Exceptions.ReportServerException("A line chart request string does not contain 'YVal=' predecate");
			//}
			//else
			//    throw new Exceptions.ReportServerException("A line chart request string does not contain 'XVal=' predecate");

			return null;
        }

        #endregion

    }
}
