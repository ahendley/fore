using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

//using National.Toolkit.Enumerators;

namespace Application.Reporting.Reports.Charting
{
    public class PieChart
    {

        #region Generate Stream

		public static MemoryStream GenerateStream(string values)
		{
			return GenerateStream(values, 32);
		}

		public static MemoryStream GenerateStream(string values, int fontSize)
		{
			Dictionary<string, double> _keyValuePairs = new Dictionary<string, double>();

			if (values.StartsWith("XVal="))
			{
				if (values.IndexOf("&YVal=") < 0)
					throw new Exception("Pie chart request error. No Y values have been provided.");

				string _xValueString = values.Substring(5, values.IndexOf("&YVal=") - 5);
				string _yValueString = values.Substring(values.IndexOf("&YVal=") + 6);

				ArrayList _xValueList = new ArrayList();
				ArrayList _yValueList = new ArrayList();
				string[] _splitValueList;
				string[] _xValueArray;
				double[] _yValueArray;

				// Get X values.
				_splitValueList = _xValueString.Split(new char[1] { '|' }, StringSplitOptions.RemoveEmptyEntries);
				foreach (string _splitValue in _splitValueList)
					_xValueList.Add(_splitValue);

				_xValueArray = (string[])_xValueList.ToArray(typeof(string));

				// Get Y values.
				_splitValueList = _yValueString.Split(new char[1] { '|' }, StringSplitOptions.RemoveEmptyEntries);
				foreach (string _splitValue in _splitValueList)
					_yValueList.Add(double.Parse(_splitValue));

				_yValueArray = (double[])_yValueList.ToArray(typeof(double));

				if (_xValueArray.Length != _yValueArray.Length)
					throw new Exception(String.Format("Pie chart request error. The number of X-values ({0}) is not equal to the number of Y-values ({1})", _xValueArray.Length, _yValueArray.Length));

				for (int _index = 0; _index < _xValueArray.Length; _index++)
					_keyValuePairs.Add(_xValueArray[_index], _yValueArray[_index]);
			}
			else
				throw new Exception("Pie chart request error. Request string does not contain 'XVal=' predecate.");

			return GenerateStream(_keyValuePairs, fontSize);
		}

		public static MemoryStream GenerateStream(Dictionary<string, double> values, int fontSize)
		{
			return PieChartUsingChartDirector(values, fontSize);
		}

		#endregion

		#region Charting Interfaces

		private static MemoryStream PieChartUsingChartDirector(Dictionary<string, double> values, int fontSize)
		{
			ChartDirector.PieChart _pieChart = null;
			MemoryStream _stream = null;

			if (values.Count > 0)
			{
				try
				{
					/* Initialise pie chart attributes */
					_pieChart = new ChartDirector.PieChart(3200, 1600);
					_pieChart.setPieSize(1600, 800, 650);
					_pieChart.setAntiAlias(true);
					_pieChart.setBackground(ChartDirector.Chart.CColor(System.Drawing.Color.White)); //, ChartDirector.Chart.CColor(System.Drawing.Color.Black), ChartDirector.Chart.CColor(System.Drawing.Color.White));
					_pieChart.setLabelFormat("{label}");
					_pieChart.setLabelLayout(ChartDirector.Chart.SideLayout);
					_pieChart.setDefaultFonts("Arial");
					_pieChart.setJoinLine(ChartDirector.Chart.CColor(System.Drawing.Color.Black), 2);
					_pieChart.setLineColor(ChartDirector.Chart.CColor(System.Drawing.Color.Black), ChartDirector.Chart.CColor(System.Drawing.Color.Black));

					ChartDirector.TextBox _labelStyle = _pieChart.setLabelStyle();
					_labelStyle.setBackground(ChartDirector.Chart.Transparent, ChartDirector.Chart.CColor(System.Drawing.Color.White));
					_labelStyle.setFontColor(ChartDirector.Chart.CColor(System.Drawing.Color.Black));
					_labelStyle.setFontSize(fontSize);
					_labelStyle.setFontStyle("Arial");

					/* Parse data for insertion into pie chart */
					Dictionary<string, double> _distributedValues = RedistributeValues(values);

					double[] _values = new double[_distributedValues.Keys.Count];
					_distributedValues.Values.CopyTo(_values, 0);

					List<System.Drawing.Color> _elementColors = new List<System.Drawing.Color>();
					List<string> _labels = new List<string>();

					foreach (string _valueName in _distributedValues.Keys)
					{
						double _value = _distributedValues[_valueName];
						string _label = string.Format("{0}%: {1}", _value.ToString("0.0#"), _valueName);
						System.Drawing.Color _elementColor = System.Drawing.Color.White;

						switch (_valueName.Trim().ToLower())
						{
							case "australian cash":
								_elementColor = System.Drawing.Color.Navy;
								break;
							case "australian property":
								_elementColor = System.Drawing.Color.OrangeRed;
								break;
							case "australian shares":
								_elementColor = System.Drawing.Color.CornflowerBlue;
								break;
							case "australian fixed interest":
								_elementColor = System.Drawing.Color.Lime;
								break;
							case "international fixed interest":
								_elementColor = System.Drawing.Color.Fuchsia;
								break;
							case "international property":
								_elementColor = System.Drawing.Color.SaddleBrown;
								break;
							case "international shares":
								_elementColor = System.Drawing.Color.Yellow;
								break;
							case "other":
								_elementColor = System.Drawing.Color.LightGray;
								break;
						}

						_elementColors.Add(_elementColor);
						_labels.Add(string.Format("{0}%: {1}", _value.ToString("0.0#"), _valueName));
					}

					/* Set the data that has now been parsed to the pie chart */
					_pieChart.setData(_values, _labels.ToArray());
					_pieChart.setColors2(ChartDirector.Chart.DataColor, ChartDirector.Chart.CColor(_elementColors.ToArray()));

					/* Get the resulting image of the pie chart */
					for (int _retry = 0; _retry < 10; _retry++)
					{
						try
						{
							_stream = new MemoryStream();
							System.Drawing.Image _image = _pieChart.makeImage();
							_image.Save(_stream, System.Drawing.Imaging.ImageFormat.Png);

							break;
						}
						catch (System.NullReferenceException)
						{
							System.Threading.Thread.Sleep(10);
						}
						catch
						{
							break;
						}
					}
				}
				finally
				{
					_pieChart = null;
				}

				return _stream;
			}
			else
				throw new Exception("Pie chart request error. There are no values pairs to generate a pie chart.");
		}

        #endregion

		#region Redistribute Data

		private static Dictionary<string, double> RedistributeValues(Dictionary<string, double> values)
		{
			Dictionary<string, double> _results = new Dictionary<string, double>();

			if (values.Count == 1)
				_results = values;
			else
			{
				List<string> _sortList = new List<string>();
				int _lowerBoundPosition = 0;
				int _upperBoundPosition = (values.Count - 1);

				foreach (string _key in values.Keys)
					_sortList.Add(string.Format("{0}|{1}", values[_key].ToString("00.0"), _key));

				_sortList.Sort();

				while (_upperBoundPosition >= _lowerBoundPosition)
				{
					string[] _upperPair = _sortList[_upperBoundPosition].Split(new char[] { '|' });
					string[] _lowerPair = _sortList[_lowerBoundPosition].Split(new char[] { '|' });

					if (_upperBoundPosition > _lowerBoundPosition)
					{
						_results.Add(_upperPair[1], double.Parse(_upperPair[0]));
						_results.Add(_lowerPair[1], double.Parse(_lowerPair[0]));
					}
					else
						_results.Add(_lowerPair[1], double.Parse(_lowerPair[0]));
						
					_upperBoundPosition--;
					_lowerBoundPosition++;
				}
			}

			return _results;
		}

		#endregion

	}
}
