﻿using Application.Reporting.FoRE;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace Application.Reporting.Reports
{
	[Report(ReportName = "Sample Report", DataType = "None", Stylesheet = "SampleReport.xslt")]
	public class SampleReport : ReportBase
	{

		#region Properties

		[ReportParameter(Name="Sub Title", Description="Enter the report sub title", DefaultValue="[sub title goes here]")]
		public string SubTitle { get; set; }

		#endregion

		#region Get XML

		public override MemoryStream GetXml()
		{
			MemoryStream _output = new MemoryStream();

			try
			{
				using (XmlWriter _writer = XmlWriter.Create(_output, this.XmlWriterSettings))
				{
					// Start of: Report
					_writer.WriteStartElement("SampleReport");

					// Start of: Header
					_writer.WriteStartElement("Header");

					_writer.WriteStartElement("Left");
					_writer.WriteString("Sample Report");
					_writer.WriteEndElement();
					_writer.WriteStartElement("Middle");
					_writer.WriteEndElement();
					_writer.WriteStartElement("Right");
					_writer.WriteString(this.GetType().FullName);
					_writer.WriteEndElement();

					// End of: Header
					_writer.WriteEndElement();

					// Start of: Body
					_writer.WriteStartElement("Body");

					_writer.WriteStartElement("Heading1");
					_writer.WriteString("Sample Report");
					_writer.WriteEndElement();
					_writer.WriteStartElement("SubTitle");
					_writer.WriteString(this.SubTitle);
					_writer.WriteEndElement();

					// Start of: Paragraphs
					_writer.WriteStartElement("Paragraphs");

					_writer.WriteStartElement("Paragraph");
					_writer.WriteString("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet consectetur ligula, id bibendum purus. Etiam eget quam lectus. Nulla leo nunc, molestie non orci et, molestie blandit nisl. Nunc dapibus, lacus interdum ullamcorper bibendum, elit turpis tincidunt mi, sed pellentesque odio mauris ac purus. Praesent suscipit dignissim lectus sed blandit. Aenean fringilla, diam eu viverra elementum, arcu augue viverra nulla, eget tempor metus magna et dui. Morbi egestas euismod massa vel luctus. Donec fringilla eleifend felis, vel scelerisque tellus varius at. Ut nec urna eget massa vulputate pulvinar. Integer viverra iaculis volutpat. Fusce ultricies lectus nisi, luctus tempus orci accumsan tristique. Nam hendrerit malesuada tempus.");
					_writer.WriteEndElement();

					_writer.WriteStartElement("Paragraph");
					_writer.WriteString("Donec posuere nisi vitae est iaculis, nec elementum urna varius. Duis vehicula lacus at feugiat pulvinar. Vivamus scelerisque metus nec lobortis eleifend. Sed et tortor nulla. Vivamus facilisis nulla non aliquet tristique. Proin vel lectus tincidunt, ultrices neque at, aliquam ipsum. Mauris elementum, enim in cursus vestibulum, purus ante pharetra magna, non mattis urna ligula eu lorem. Curabitur id sollicitudin libero.");
					_writer.WriteEndElement();

					_writer.WriteStartElement("Paragraph");
					_writer.WriteString("In sed nisl mollis, placerat massa sit amet, sagittis eros. Cras rutrum commodo viverra. Pellentesque vestibulum ligula sed consectetur egestas. Sed at augue at nibh porttitor auctor ac ac justo. Suspendisse consequat erat sit amet dui tincidunt, et faucibus dui eleifend. Sed quis pretium turpis. Duis quis diam sed urna ornare ultrices. Quisque ligula augue, volutpat in justo eu, congue fermentum mi. Vivamus lorem erat, tincidunt id mattis sed, facilisis vitae justo. Nam vitae purus lacus. Morbi tincidunt justo eu nisi blandit eleifend.");
					_writer.WriteEndElement();

					// End of: Paragraphs
					_writer.WriteEndElement();

					// End of: Body
					_writer.WriteEndElement();

					// Start of: Footer
					_writer.WriteStartElement("Footer");

					_writer.WriteStartElement("Left");
					_writer.WriteEndElement();
					_writer.WriteStartElement("Middle");
					_writer.WriteEndElement();
					_writer.WriteStartElement("Right");
					_writer.WriteEndElement();

					// End of: Footer
					_writer.WriteEndElement();

					// End of: Report
					_writer.WriteEndElement();

					_writer.Flush();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}

			return _output;
		}

		#endregion

	}
}
