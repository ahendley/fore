﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using National.Reporting.foRendererEngine;
using System.IO;
using System.Xml;

namespace National.Reporting.Reports
{

	[Report(ReportName = "Data Integrity: Dashboard Report", DataType = "Oracle", Stylesheet = "DataIntegrityDashboardReport.xslt")]
	public class DataIntegrityDashboardReport : ReportBase
	{

		public override MemoryStream GetXml()
		{
			MemoryStream _output = new MemoryStream();

			try
			{
				using (XmlWriter _writer = XmlWriter.Create(_output, this.XmlWriterSettings))
				{
					_writer.WriteStartElement("DataIntegrityDashboardReport");

					_writer.WriteStartElement("Header");
					_writer.WriteStartElement("Left");
					_writer.WriteEndElement();
					_writer.WriteStartElement("Middle");
					_writer.WriteEndElement();
					_writer.WriteStartElement("Right");
					_writer.WriteEndElement();
					_writer.WriteEndElement();

					_writer.WriteStartElement("Body");
					_writer.WriteStartElement("Title");
					_writer.WriteString("Data Integrity: Dashboard Report");
					_writer.WriteEndElement();
					_writer.WriteEndElement();

					_writer.WriteStartElement("Footer");
					_writer.WriteStartElement("Left");
					_writer.WriteEndElement();
					_writer.WriteStartElement("Middle");
					_writer.WriteEndElement();
					_writer.WriteStartElement("Right");
					_writer.WriteEndElement();
					_writer.WriteEndElement();

					_writer.WriteEndElement();

					_writer.Flush();
				}

				_output.Position = 0;
			}
			catch (Exception ex)
			{
				throw ex;
			}

			return _output;
		}

	}

}
