﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Reporting.FoRE
{
	[AttributeUsage(AttributeTargets.Property)]
	public class ReportParameterAttribute : Attribute
	{
		public virtual string Name { get; set; }
		public virtual string Description { get; set; }
		public virtual object DefaultValue { get; set; }

		public ReportParameterAttribute()
		{ }

		public ReportParameterAttribute(string name, string description, object defaultValue)
		{
			this.Name = name;
			this.Description = description;
			this.DefaultValue = defaultValue;
		}
	}
}
