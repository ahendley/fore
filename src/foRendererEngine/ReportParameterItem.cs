﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Reporting.FoRE
{
	public class ReportParameterItem
	{

		public string Name { get; private set; }
		public string Descrption { get; private set; }
		public object Value { get; set; }

		public ReportParameterItem(string name, string description, object value)
		{
			this.Name = name;
			this.Descrption = description;
			this.Value = value;
		}

	}
}
