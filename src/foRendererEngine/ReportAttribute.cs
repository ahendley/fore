﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Reporting.FoRE
{

	[AttributeUsage(AttributeTargets.Class)]
	public class ReportAttribute : System.Attribute 
	{

		public virtual string ReportName { get; set; }
		public virtual string DataType { get; set; }
		public virtual string Stylesheet { get; set; }

		public ReportAttribute()
		{
			this.ReportName = string.Empty;
			this.DataType = string.Empty;
		}

		public ReportAttribute(string reportName, string dataType, string stylesheet)
		{
			this.ReportName = reportName;
			this.DataType = dataType;
			this.Stylesheet = stylesheet;
		}

	}
}
