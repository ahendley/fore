﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.IO;
using System.Drawing;
using Fonet.Render.Pdf;
using Fonet;
using System.Xml;
using System.Xml.Xsl;
using System.Drawing.Imaging;

namespace Application.Reporting.FoRE
{
	public static class RendererEngine
	{

		#region Fields

		private static readonly ILog Log = LogManager.GetLogger(typeof(RendererEngine));
		private static Dictionary<string, object> ImagesCache = new Dictionary<string, object>();
		private static Dictionary<string, object> StylesheetTransformers = new Dictionary<string, object>();
		private static string _workingFolder = string.Empty;
		private static string _xmlOutputFolder = string.Empty;
		private static string _xslfoOutputFolder = string.Empty;

		#endregion

		#region Properties

		public static bool CacheImagesInMemory { get; set; }
		public static bool SaveXMLOutput { get; set; }
		public static bool SaveXSLFOOutput { get; set; }
		public static string ImagesFolder { get; set; }
		public static Encoding Encoding = new UTF8Encoding();

		public static string WorkingFolder
		{
			get
			{
				if (string.IsNullOrWhiteSpace(_workingFolder))
					_workingFolder = Path.Combine(Path.GetTempPath(), "foRendererEngine");

				if (!Directory.Exists(_workingFolder))
					Directory.CreateDirectory(_workingFolder);

				return _workingFolder;
			}
			set
			{
				_workingFolder = value;

				if (!Directory.Exists(_workingFolder))
					Directory.CreateDirectory(_workingFolder);
			}
		}

		private static string XmlOutputFolder
		{
			get
			{
				if (string.IsNullOrWhiteSpace(_xmlOutputFolder))
					_xmlOutputFolder = Path.Combine(WorkingFolder, "xml");

				if (!Directory.Exists(_xmlOutputFolder))
					Directory.CreateDirectory(_xmlOutputFolder);

				return _xmlOutputFolder;
			}
		}

		private static string XslfoOutputFolder
		{
			get
			{
				if (string.IsNullOrWhiteSpace(_xslfoOutputFolder))
					_xslfoOutputFolder = Path.Combine(WorkingFolder, "xslfo");

				if (!Directory.Exists(_xslfoOutputFolder))
					Directory.CreateDirectory(_xslfoOutputFolder);

				return _xslfoOutputFolder;
			}
		}

		#endregion

		#region Process Functions

		public static void Process(ReportItem reportItem, Stream outputStream)
		{
			Process(reportItem, outputStream, new RenderingOptions());
		}

		public static void Process(ReportItem reportItem, Stream outputStream, RenderingOptions options)
		{
			//XslCompiledTransform _xslTransformer = null;
			XslTransform _xslTransformer = null;
			FonetDriver _fonetDriver = null;
			PdfRendererOptions _pdfOptions = null;
			string _guidString = System.Guid.NewGuid().ToString();

			try
			{
				string _key = reportItem.Stylesheet.ToLower().GetHashCode().ToString();
				if (StylesheetTransformers.ContainsKey(_key))
				{
					//_xslTransformer = (XslCompiledTransform)StylesheetTransformers[_key];
					_xslTransformer = (XslTransform)StylesheetTransformers[_key];
				}
				else
				{
					//XsltSettings _xsltSettings = new XsltSettings();
					//_xsltSettings.EnableDocumentFunction = true;
					//_xsltSettings.EnableScript = true;

					//XmlResolver _resolver = new XmlUrlResolver();

					//_xslTransformer = new XslCompiledTransform();
					//_xslTransformer.Load(stylesheetFilename, _xsltSettings, _resolver);

					_xslTransformer = new XslTransform();
					_xslTransformer.Load(reportItem.Stylesheet);

					StylesheetTransformers.Add(_key, _xslTransformer);
				}

				XmlDocument _xmlDocument = new XmlDocument();
				IReport _iReport = (IReport)Activator.CreateInstance(reportItem.Type);

				using (Stream _xmlStream = _iReport.GetXml())
				{
					_xmlStream.Position = 0;
					_xmlDocument.Load(_xmlStream);

					if (SaveXMLOutput)
					{
						string _xmlFilename = Path.Combine(XmlOutputFolder, string.Format("{0}.xml", _guidString));
						_xmlDocument.Save(_xmlFilename);
					}
				}

				using (MemoryStream _xslfoStream = new MemoryStream())
				{
					_xslTransformer.Transform(_xmlDocument, null, _xslfoStream);

					if (SaveXSLFOOutput)
					{
						string _filename = Path.Combine(XslfoOutputFolder, string.Format("{0}.xslfo", _guidString));
						byte[] _bytes = _xslfoStream.ToArray();

						using (FileStream _xslfoFile = File.Open(_filename, FileMode.Create, FileAccess.Write, FileShare.Write))
						{
							_xslfoFile.Write(_bytes, 0, _bytes.Length);
						}
					}

					_xslfoStream.Position = 0;

					_pdfOptions = new PdfRendererOptions();
					_pdfOptions.Title = options.Title;
					_pdfOptions.Author = options.Author;
					_pdfOptions.EnableAdd = options.AllowAdd;
					_pdfOptions.EnableCopy = options.AllowCopy;
					_pdfOptions.EnableModify = options.AllowModify;
					_pdfOptions.EnablePrinting = options.AllowPrinting;
					_pdfOptions.Kerning = options.FontKerningOn;
					_pdfOptions.OwnerPassword = (string.IsNullOrWhiteSpace(options.OwnerPassword) ? null : options.OwnerPassword);
					_pdfOptions.UserPassword = (string.IsNullOrWhiteSpace(options.UserPassword) ? null : options.UserPassword);
					foreach (string _keyword in options.Keywords)
						_pdfOptions.AddKeyword(_keyword);

					_fonetDriver = Fonet.FonetDriver.Make();
					_fonetDriver.BaseDirectory = new DirectoryInfo(WorkingFolder);
					_fonetDriver.CloseOnExit = false;
					_fonetDriver.Options = _pdfOptions;

					Type _imageHandler = (Type)AvailableReports.GetAssembyImageHandler(_iReport.GetType().Assembly.FullName);
					if (_imageHandler != null)
					{
						Log.DebugFormat("Creating instance of image handler to instance of - {0}", _imageHandler.FullName);
						IImageHandler _handler = (IImageHandler)Activator.CreateInstance(_imageHandler);
						Log.DebugFormat("Setting the image handler instance ({0}) root folder to - {1}", _handler.GetType().FullName, ImagesFolder);
						_handler.RootFolder = ImagesFolder;
						Log.DebugFormat("Setting the image handler instance ({0}) cache images setting to - {1}", _handler.GetType().FullName, CacheImagesInMemory);
						_handler.CacheImages = CacheImagesInMemory;

						_fonetDriver.ImageHandler = new FonetDriver.FonetImageHandler(_handler.GetImageData);
					}
					else
						_fonetDriver.ImageHandler = new Fonet.FonetDriver.FonetImageHandler(FonetImageHandler);

					Log.Debug("Rendering FO data to PDF stream");
					DateTime _startProcessing = DateTime.Now;

					_fonetDriver.Render(_xslfoStream, outputStream);

					DateTime _stopProcessing = DateTime.Now;
					TimeSpan _processingTime = (_stopProcessing - _startProcessing);

					Log.InfoFormat("Rendering of FO data to PDF stream elapsed time - {0}", _processingTime);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				_fonetDriver = null;
				_pdfOptions = null;
				_xslTransformer = null;
			}
		}

		#endregion

		#region Image Handler

		private static byte[] FonetImageHandler(string value)
		{
			Log.DebugFormat("Rendering image with data ({0})", value);

			MemoryStream _memoryStream = new MemoryStream();
			string _localFilename = Path.Combine(ImagesFolder, value);

			if (CacheImagesInMemory)
			{
				if (ImagesCache.ContainsKey(_localFilename))
					_memoryStream = (MemoryStream)ImagesCache[_localFilename];
				else
				{
					_memoryStream = GetLocalImageFile(_localFilename);
					ImagesCache.Add(_localFilename, _memoryStream);
				}
			}
			else
			{
				_memoryStream = GetLocalImageFile(_localFilename);
			}

			return _memoryStream.ToArray();
		}

		private static MemoryStream GetLocalImageFile(string filename)
		{
			MemoryStream _stream = new MemoryStream();

			using (Image _image = Image.FromFile(filename))
			{
				_image.Save(_stream, ImageFormat.Png);
			}

			return _stream;
		}

		#endregion

	}
}
