﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Reporting.FoRE
{
	public interface IImageHandler
	{

		string RootFolder { get; set; }
		bool CacheImages { get; set; }

		byte[] GetImageData(string value);

	}
}
