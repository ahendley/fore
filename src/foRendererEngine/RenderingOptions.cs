﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Reporting.FoRE
{
	public class RenderingOptions
	{
		public string Title { get; set; }
		public string Subject { get; set; }
		public string Author { get; set; }
		public string[] Keywords { get; set; }
		public bool AllowAdd { get; set; }
		public bool AllowCopy { get; set; }
		public bool AllowModify { get; set; }
		public bool AllowPrinting { get; set; }
		public bool FontKerningOn { get; set; }
		public string OwnerPassword { get; set; }
		public string UserPassword { get; set; }

		public RenderingOptions()
		{
			this.Title = string.Empty;
			this.Subject = string.Empty;
			this.Author = string.Empty;
			this.Keywords = new string[0];
			this.AllowAdd = true;
			this.AllowCopy = true;
			this.AllowModify = true;
			this.AllowPrinting = true;
			this.FontKerningOn = true;
			this.OwnerPassword = string.Empty;
			this.UserPassword = string.Empty;
		}
	}
}
