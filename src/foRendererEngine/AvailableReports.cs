﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Reflection;
using System.IO;
using log4net;

namespace Application.Reporting.FoRE
{
	public static class AvailableReports
	{

		#region Fields

		private static readonly ILog Log = LogManager.GetLogger(typeof(AvailableReports));
		private static string ConfigFilename = "AvailableReports.config";

		//Change to a Dictionary<string, ReportItem>
		private static List<ReportItem> knownReports = new List<ReportItem>();
		private static List<Assembly> knownAssemblies = new List<Assembly>();
		private static Dictionary<string, object> knownAssemblyImageHandlers = new Dictionary<string, object>();

		#endregion

		#region Properties

		public static List<Assembly> Assemblies
		{
			get { return knownAssemblies; }
		}

		public static List<ReportItem> Reports
		{
			get { return knownReports; }
		}

		#endregion

		#region Constructor

		static AvailableReports()
		{ 
		}

		#endregion

		#region Initialise

		public static void Initialise()
		{
			XmlDocument _doc = null;

			try
			{
				_doc = new XmlDocument();
				_doc.Load(AvailableReports.ConfigFilename);

				XmlNodeList _nodes = _doc.SelectNodes("/AvailableReports/Report");
				foreach (XmlNode _node in _nodes)
				{
					if (_node.Attributes.Count < 1)
						throw new XmlException("A report node attribute \"/AvailableReports/Report[@key]\" is missing.");

					if (string.IsNullOrWhiteSpace(_node.InnerText))
						throw new XmlException("A report node \"/AvailableReports/Report\" value is missing.");

					string _key = _node.Attributes[0].Value;
					string _typeName = _node.InnerText;
					Type _type = Type.GetType(_typeName);
					ReportItem _reportItem = null;

					if (_type != null)
					{
						ReportAttribute _reportAttribute = (ReportAttribute)Attribute.GetCustomAttribute(_type, typeof(ReportAttribute));

						if (_reportAttribute != null)
							_reportItem = new ReportItem(_key, _reportAttribute.ReportName, _type, _reportAttribute.DataType, _reportAttribute.Stylesheet);
					}

					if (_reportItem != null)
					{
						knownReports.Add(_reportItem);

						if (!knownAssemblies.Contains(_reportItem.Type.Assembly))
						{
							knownAssemblies.Add(_reportItem.Type.Assembly);

							foreach (Type _t in _reportItem.Type.Assembly.GetTypes())
							{
								var _t2 = _t.GetInterface(typeof(IImageHandler).Name);

								if (_t2 == typeof(IImageHandler))
								{
									knownAssemblyImageHandlers.Add(_reportItem.Type.Assembly.FullName, _t);
									// break here as only one image handler instance is allowed per report assembly.
									break;
								}
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				_doc = null;
			}
		}

		public static void Initialise(string configFilename)
		{
			AvailableReports.ConfigFilename = configFilename;
			AvailableReports.Initialise();
		}

		#endregion

		#region Extract

		public static void ExtractFiles(string rootFolder)
		{
			foreach (Assembly _assembly in knownAssemblies)
			{
				Log.InfoFormat("Extracting files from assembly ({0}) to path - {1}", _assembly.FullName, rootFolder);

				string _commonPath = GetCommonPath(_assembly);
				string _assemblyName = _assembly.GetName().Name;
				Log.DebugFormat("Assembly common path identified as - {0}", _commonPath);

				List<string> _resourceNames = new List<string>(_assembly.GetManifestResourceNames());

				foreach (string _resourceName in _resourceNames)
				{
					Log.InfoFormat("Processing embedded resource - {0}", _resourceName);

					byte[] _bytes;

					using (Stream _stream = _assembly.GetManifestResourceStream(_resourceName))
					{
						_bytes = new byte[_stream.Length];
						_stream.Read(_bytes, 0, _bytes.Length);
					}

					string _name = _resourceName.Substring(_commonPath.Length).Replace('.', System.IO.Path.DirectorySeparatorChar);
					_name = _name.Substring(0, _name.LastIndexOf(System.IO.Path.DirectorySeparatorChar)) + "." + _name.Substring(_name.LastIndexOf(System.IO.Path.DirectorySeparatorChar) + 1);
					string _fileName = Path.Combine(Path.Combine(rootFolder, _assemblyName), _name);

					Log.InfoFormat("Saving embedded resource ({0}) to disk as - {1}", _resourceName, _fileName);

					if (!Directory.Exists(Path.GetDirectoryName(_fileName)))
						Directory.CreateDirectory(Path.GetDirectoryName(_fileName));

					using (FileStream _fileStream = new FileStream(_fileName, FileMode.Create, FileAccess.Write, FileShare.Write))
					{
						_fileStream.Write(_bytes, 0, _bytes.Length);
					}

					foreach (ReportItem _reportItem in AvailableReports.Reports)
					{
						if (_fileName.ToLower().EndsWith(_reportItem.Stylesheet.ToLower()))
						{
							_reportItem.Stylesheet = _fileName;
							break;
						}
					}
				}

			}
		}

		#endregion

		#region Common Path Functions

		public static string GetCommonPath(Assembly assembly)
		{
			return GetCommonPath(assembly.GetManifestResourceNames());
		}

		public static string GetCommonPath(IEnumerable<string> strings)
		{
			var commonPrefix = strings.FirstOrDefault() ?? "";

			foreach (var s in strings)
			{
				var potentialMatchLength = Math.Min(s.Length, commonPrefix.Length);

				if (potentialMatchLength < commonPrefix.Length)
					commonPrefix = commonPrefix.Substring(0, potentialMatchLength);

				for (var i = 0; i < potentialMatchLength; i++)
				{
					if (s[i] != commonPrefix[i])
					{
						commonPrefix = commonPrefix.Substring(0, i);
						break;
					}
				}
			}

			return commonPrefix;
		}

		#endregion

		public static object GetAssembyImageHandler(string assemblyName)
		{
			object _object = null;

			if (knownAssemblyImageHandlers.ContainsKey(assemblyName))
				_object = knownAssemblyImageHandlers[assemblyName];

			return _object;
		}


	}
}
