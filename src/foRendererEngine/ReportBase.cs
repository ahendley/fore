﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Reflection;

namespace Application.Reporting.FoRE
{
	public class ReportBase : IReport
	{

		#region Fields

		private XmlWriterSettings xmlWriterSettings = null;
		protected string connectionString = string.Empty;
		protected string defaultSchema = string.Empty;

		#endregion

		#region Constructor

		public ReportBase()
		{
			this.InitialiseXmlWriterSettings();
		}

		#endregion

		#region Initialise Functions

		private void InitialiseXmlWriterSettings()
		{
			this.xmlWriterSettings = new XmlWriterSettings();
			this.xmlWriterSettings.CloseOutput = false;
			this.xmlWriterSettings.ConformanceLevel = ConformanceLevel.Document;
			this.xmlWriterSettings.Encoding = RendererEngine.Encoding;
			this.xmlWriterSettings.Indent = true;
			this.xmlWriterSettings.IndentChars = "\t";
			this.xmlWriterSettings.NamespaceHandling = NamespaceHandling.OmitDuplicates;
			this.xmlWriterSettings.NewLineHandling = NewLineHandling.Entitize;
			this.xmlWriterSettings.NewLineOnAttributes = false;
			this.xmlWriterSettings.OmitXmlDeclaration = false;
		}

		#endregion

		#region Properties

		public XmlWriterSettings XmlWriterSettings
		{
			get { return this.xmlWriterSettings; }
			set { this.xmlWriterSettings = value; }
		}

		#endregion

		#region GetXml Functions

		public virtual MemoryStream GetXml()
		{
			return new MemoryStream();
		}

		public MemoryStream GetXml(string connectionString)
		{
			this.connectionString = connectionString;

			return this.GetXml();
		}

		public MemoryStream GetXml(string dataSource, string userName, string password, string database, string schema)
		{
			Dictionary<string, object> _params = new Dictionary<string, object>();

			_params.Add("Data Source", dataSource);
			_params.Add("User ID", userName);
			_params.Add("Password", password);
			if (!string.IsNullOrWhiteSpace(database))
				_params.Add("Database", database);

			this.defaultSchema = schema;

			return this.GetXml(_params);
		}

		public MemoryStream GetXml(Dictionary<string, object> connectionParams)
		{
			StringBuilder _connectionString = new StringBuilder();

			foreach (KeyValuePair<string, object> _pair in connectionParams)
			{
				if (_pair.Key.ToLower().Equals("schema"))
					this.defaultSchema = _pair.Value.ToString();
				else
					_connectionString.AppendFormat("{0}={1};", _pair.Key, _pair.Value);
			}

			this.connectionString = _connectionString.ToString();

			return this.GetXml();
		}

		#endregion

		#region Get Image Data

		public virtual byte[] GetImageDataCallback(string value)
		{
			return new byte[0];
		}

		#endregion

	}
}
