﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Application.Reporting.FoRE
{
	public interface IReport
	{
		MemoryStream GetXml();
		MemoryStream GetXml(string connectionString);
		MemoryStream GetXml(string dataSource, string userName, string password, string database, string schema);
		MemoryStream GetXml(Dictionary<string, object> connectionParams);

		byte[] GetImageDataCallback(string value);
	}
}
