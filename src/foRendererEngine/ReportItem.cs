﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Application.Reporting.FoRE
{
	public class ReportItem
	{

		public string Key { get; private set; }
		public string Name { get; private set; }
		public Type Type { get; private set; }
		public string DataType { get; private set; }
		public string Stylesheet { get; internal set; }
		public Dictionary<string, ReportParameterItem> Parameters { get; private set; }

		public ReportItem(string key, string name, Type type, string dataType, string stylesheet)
		{
			this.Key = key;
			this.Name = name;
			this.Type = type;
			this.DataType = dataType;
			this.Stylesheet = stylesheet;
			this.Parameters = new Dictionary<string, ReportParameterItem>();

			foreach (MemberInfo _info in this.Type.GetMembers())
			{
				if (_info.MemberType == MemberTypes.Property)
				{
					var _attributes = _info.GetCustomAttributes(typeof(ReportParameterAttribute), false);

					foreach (Attribute _attribute in _attributes)
					{
						if (_attribute.GetType() == typeof(ReportParameterAttribute))
						{
							ReportParameterAttribute _param = (ReportParameterAttribute)_attribute;
							this.Parameters.Add(_info.Name, new ReportParameterItem(_param.Name, _param.Description, _param.DefaultValue));
							break;
						}
					}
				}
			}
		}

		public override string ToString()
		{
			return this.Name;
		}

	}
}
